import React from 'react'

import './Hello.less'

class Hello extends React.Component{
    render(){
        return(
            <div className="background">
                <div className="fondInt">
                    Internship 2020 DevOps
                </div>
                <div className="imgContainer">
                    <img src="https://i.imgur.com/oQhzN7o.jpeg" alt=""/>
                </div>
                <div className="aws">
                    AWS
                </div>
                <div className="azure">
                    AZURE
                </div>
            </div>
            
        );
    }
}

export default Hello
